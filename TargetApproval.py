#!usr/bin/python
import unittest
import CommonUtensils
import NoRedirection
import time


# class responsible for targetting check
class TargetApproval(unittest.TestCase):
	
	# def __init__(self):
	# 	pass

	def __init__(self, *args, **kwargs):
		unittest.TestCase.__init__(self, *args, **kwargs)

	def runTest(self):
		pass

	def setUp(self):
		self.environment_is_clean()

	def environment_is_clean(self):
		try:
			self.assertEqual(hardcodedParams['Expectedreason'],hardcodedParams['reason'],msg='correct reason is not logged for targetting approval')
		except Exception as e:
			 self.skipTest(test_fallBackUrl)

			

	global hardcodedParams 
	hardcodedParams 					= {} 	
	hardcodedParams['bannerID'] 		= '17640394'
	hardcodedParams['affiliateID']  	= '15927'
	hardcodedParams['Expectedreason']	= 0


	# method responsible for redirection and getting redirecting data
	def getRedirectedData(self):
		result 							= CommonUtensils.common.getActualUrl(NoRedirection.class_instance)
		hardcodedParams['cookies']		= result['cookie']
		hardcodedParams['ut_cookies']	= result['ut_cookie']
		hardcodedParams['actual_URL'] 	= result['targetURL']
		#time.sleep(60)
		collection 						= 'ox_raw_clicks_new'
		condition  						= {"userTrackingCookie":hardcodedParams['ut_cookies'],"affiliate_id":hardcodedParams['affiliateID'],"ad_id":hardcodedParams['bannerID']}
		result 	   						= CommonUtensils.common.mongoData(collection,condition)
		for r in result:
			reason 						= r['reason_for_redirection']
			reason 						= str(reason)
			hardcodedParams['reason'] 	= reason
		self.getFallBackURl()

	
	# method for getting fall back url
	def getFallBackURl(self):
		url 									= ''
		bannerDetails  							= CommonUtensils.common.getBannerDetails(hardcodedParams['bannerID'])
		hardcodedParams['campaignID'] 			= str(bannerDetails[0][6])
		hardcodedParams['discontinueUrl'] 		= bannerDetails[0][14]
		url 									= CommonUtensils.common.getGeoUrl(hardcodedParams['campaignID'],hardcodedParams['Expectedreason'],hardcodedParams['affiliateID'])
		if not url:
			url 								= hardcodedParams['discontinueUrl']
			if not url:
				url 							= CommonUtensils.common.getGlobalUrl()
		edits 								    = [("%%campaignId%%",hardcodedParams['campaignID']),("%%affiliateId%%",hardcodedParams['affiliateID']), ("%%bannerId%%",hardcodedParams['bannerID']),("%%reasonForRedirection%%",hardcodedParams['Expectedreason'])]
		try:
			for search, replace in edits:
	 			url  									= url.replace(search, replace)
	 			url 									= url.replace("'","")
	 			hardcodedParams['ExpectedFallBackURL'] 	= url
	 	except:
			hardcodedParams['ExpectedFallBackURL'] 	= url


	# def test_reason(self):
	# 	'''testing targetting expected reason 4 with actual'''
	# 	self.assertEqual(hardcodedParams['reason'],hardcodedParams['Expectedreason'],msg='correct reason is not logged for targetting approval')
	
	def test_fallBackUrl(self):
		''' testing for fallback url'''
		self.assertEqual(hardcodedParams['ExpectedFallBackURL'],hardcodedParams['actual_URL'],msg='actual url does not match with expected url')


targetApp  = TargetApproval()	

if __name__ == '__main__':
    #unittest.main()
	target  = TargetApproval()
	target.getRedirectedData()
	unittest.main()