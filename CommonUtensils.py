#!usr/bin/python
import MySQLdb
import pymongo
import urllib2
import cookielib
from pymongo import MongoClient

# class will contain all common methods.
class CommonUtensil:
	def __init__(self):
		pass

	# method to get mysql data
	def mySQLData(self,query):
		dbSql 				= MySQLdb.connect(host='localhost',user='root',passwd='tyroo123',db='openx')
		cursor 				= dbSql.cursor()
		cursor.execute(query)
		dest 				= cursor.fetchall()
		return dest

	# method to get mongo data
	def mongoData(self,collection,condition):
		client 				= MongoClient()
		db 					= client['openx']
		collection 			= db[collection]
		response 			= collection.find(condition).sort("_id",pymongo.DESCENDING).limit(1)# getting latest document
		return response

	# method to get global url
	def getGlobalUrl(self):
		query 				= 'SELECT url FROM ox_globalredirection'
		response 			= []
		response 			=  mySQLData(query)
		response 			= ''.join(response[0])
		#response 			= response.split('?')[0]
		return response

	# method to get geo Url .
	def getGeoUrl(self,campaignID,reason,affiliateID):
		collection 			= 'ox_raw_clicks_new'
		condition 			= {"campaign_id":str(campaignID),"affiliate_id":str(affiliateID),"reason_for_redirection":reason}
		result 				= self.mongoData(collection,condition)
		for r in result:
			countryCode     = r['country']
			countryCode 	= str(countryCode)
		query 				= 'SELECT url FROM ox_georedirection WHERE campaignid = "%s" AND country = "%s"' %(campaignID,countryCode)
		response 			= []
		response 			= self.mySQLData(query)
		if response:
			response 			= ''.join(response[0])
		return response

	
	# gets banner details by given bannerID
	def getBannerDetails(self,bannerID):
		query = """SELECT b.bannerid, b.url_mobile, b.url, b.compiledlimitation, b.acl_plugins,c.enable_targeting, c.campaignid, 
				   c.visibility, c.deeplink_query_string,c.status, c.deeplink_condition, c.cookietime, c.clientid, 
				   t.trackerid, c.discontinue_url, c.expire_time,c.views as total_impressions, c.clicks as total_clicks,c.conversions as total_conversions,c.daily_impressions, c.daily_clicks, c.daily_conversions, 
				   c.monthly_impressions, c.monthly_clicks, c.monthly_conversions FROM ox_banners as b 
				   INNER JOIN ox_campaigns as c  ON (b.campaignid = c.campaignid)INNER JOIN  ox_campaigns_trackers as t ON (t.campaignid = c.campaignid)
				   WHERE b.bannerid = "%s";""" % (bannerID)
		bannerDetail  		= self.mySQLData(query)# get result from mysql 's openx db'
		bannerdet 			= list(bannerDetail)
		return bannerdet

	# gets affiliate details
	def getAffiliateDetails(self,affiliateID,adgroupID):
		query 				= """SELECT a.affiliateid, a.account_id as accountid, z.zoneid as zone_id, z.delivery, z.status FROM ox_affiliates as a RIGHT OUTER JOIN ox_zones as z ON (a.affiliateid = z.affiliateid) WHERE (a.affiliateid = "%s" AND z.delivery = '-1') OR (z.zoneid = "%s");""" % (affiliateID,adgroupID)
		affiliateDetail 	= self.mySQLData(query)
		affDetails 			= list(affiliateDetail)
		return affDetails

	# method responsible for redirection and getting actual url
	def getActualUrl(self,noRe):
		cookies 			= {}
		result 			   	= {}
		cj 				   	= cookielib.CookieJar()
		result['coolieObj'] = cj
		opener 			   	= urllib2.build_opener(noRe, urllib2.HTTPCookieProcessor(cj))
		ckt 			   	= "http://localhost/tyroocentral-platform/www/delivery/ckt.php?bannerid=17640394&affid=15927&subid1=&subid2=&subid3=&subid4=&subid5=&productid=ACCDCUC3STF7NPKD&optionalParams=&requestId=575fb5576b61c1465890135575fb5576b6cf&requestSource=PUBLIC&ad_group_id=-15927&gaid=d4f8030b-4a52-4261-9830-bd6c987cd261&IDFA=&dest=http://dl.flipkart.com/dl/philips-she-3590bk-98-headphone/p/itmdcudfaghsbmyj?pid=ACCDCUC3STF7NPKD&affid=svgmedia&affExtParam1=15927%3B%%trackerId%%%3B%%cookieId%%%3B%%subId1%%%3B%%subId2%%%3B%%subId3%%%3B%%subId4%%%3B%%subId5%%%3B&affExtParam2=%%cookieId%%"
		response 		   	= opener.open(ckt)
		for cookie in cj:
   			cookies[cookie.name] 	= cookie.value
   		if cookies:
   			oaid_cookie 		= cookies['OAID']
			ut_cookie 			= cookies['__tyroo_ut']
			actCookie 			= oaid_cookie+'_UT_'+ut_cookie
			result['cookie'] 	= actCookie
			result['ut_cookie']	= ut_cookie
		if response.code   == 302:
			redirection_target 	= response.headers['Location']
			result["targetURL"]	= redirection_target 
		return result

common  = CommonUtensil()