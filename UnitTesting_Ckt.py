#!usr/bin/python
import CampaignApproval
import CampaignCapping
import TargetApproval
import UserCapping
import ValidCKT
import AdwallStatus_Inactive
import unittest

class UnitTesting_Ckt(unittest.TestCase):

	def __init__(self, *args, **kwargs):
		unittest.TestCase.__init__(self, *args, **kwargs)

	def runTest(self):
		pass
	

	def setUp(self):
		print "------------------------ Target Approval---------------------------"
		TargetApproval.targetApp.getRedirectedData()
		TargetApproval.targetApp.environment_is_clean()

		print "------------------------ Campaign Approval-------------------------"
		CampaignApproval.campaignApp.getRedirectedData()
		CampaignApproval.campaignApp.environment_is_clean()

		print"------------------------- CampaignCapping---------------------------"
		CampaignCapping.campaignCapping.getRedirectedData()
		CampaignCapping.campaignCapping.environment_is_clean()

		print"------------------------- User Capping------------------------------"
		UserCapping.userCapping.getRedirectedData()
		UserCapping.userCapping.environment_is_clean()

		print "------------------------ CKT Validation----------------------------"
		ValidCKT.validCkt.getRedirectedData()
		ValidCKT.validCkt.environment_is_clean()

		print "------------------------- Adwall status Inactive-------------------"
		AdwallStatus_Inactive.adWall.getRedirectedData()
		AdwallStatus_Inactive.adWall.environment_is_clean()

if __name__ == '__main__':
	print "..........*****unit Testing****-----------"
	unittest.main()
	
	