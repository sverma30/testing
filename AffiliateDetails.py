#!usr/bin/python
import CommonUtensils

#class responsible for affiliate 
class AffliateDetail:

	# method to get affiliate details
	def getAffiliateDetails(affiliateID,adgroupID):
		query 				= """SELECT a.affiliateid, a.account_id as accountid, z.zoneid as zone_id, z.delivery, z.status FROM ox_affiliates as a RIGHT OUTER JOIN ox_zones as z ON (a.affiliateid = z.affiliateid) WHERE (a.affiliateid = "%s" AND z.delivery = '-1') OR (z.zoneid = "%s");""" % (affiliateID,adgroupID)
		affiliateDetail 	= getMysqlResult(query)
		affDetails 			= list(affiliateDetail)
		return affDetails
 