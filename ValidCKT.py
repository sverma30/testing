#!usr/bin/python
import unittest
import CommonUtensils
import NoRedirection
import urlparse
import time

# class responsible for checking ckt validation
class ValidCkt(unittest.TestCase):

	def __init__(self, *args, **kwargs):
		unittest.TestCase.__init__(self, *args, **kwargs)

	def runTest(self):
		pass

	def setUp(self):
		self.environment_is_clean()

	def environment_is_clean(self):
		self.assertEqual(hardcodedParams['ExpectedDestURl'],hardcodedParams['actual_URL'],msg="url matching failed")

	
	# global variable contains expected value
	global hardcodedParams 
	hardcodedParams 							= {} 	
	hardcodedParams['bannerID'] 				= '17640394'
	hardcodedParams['affiliateID']  			= '15927'
	hardcodedParams['ad_groupID'] 				= '-15927'
	hardcodedParams['Expectedreason']			= 0


	# method responsible for redirection and getting redirecting data
	def getRedirectedData(self):
		result 							= CommonUtensils.common.getActualUrl(NoRedirection.class_instance)
		hardcodedParams['cookies']		= result['cookie']
		hardcodedParams['ut_cookies']	= result['ut_cookie']
		hardcodedParams['actual_URL'] 	= result['targetURL']
		#time.sleep(60)
		collection 						= 'ox_raw_clicks_new'
		condition  						= {"userTrackingCookie":hardcodedParams['ut_cookies'],"affiliate_id":hardcodedParams['affiliateID'],"ad_id":hardcodedParams['bannerID']}
		result 	   						= CommonUtensils.common.mongoData(collection,condition)
		for r in result:
			reason 						= r['reason_for_redirection']
			reason 						= str(reason)
			hardcodedParams['reason'] 	= reason
		bannerDetails  					= CommonUtensils.common.getBannerDetails(hardcodedParams['bannerID'])
		hardcodedParams['trackerID']    = str(bannerDetails[0][13])
		hardcodedParams['campaignID'] 	= str(bannerDetails[0][6])
		affiliateDetails  				= CommonUtensils.common.getAffiliateDetails(hardcodedParams['affiliateID'],hardcodedParams['ad_groupID'])
		hardcodedParams['zoneID'] 		= affiliateDetails[0][2]
		self.replaceMacroos()

	# method for replacing macros
	def replaceMacroos(self):
		subID1 = subID2 =subID3 = subID4 = subID5 = optionalParams = IDFA= gaid =click_ID= ''
		ckt 								= 'http://localhost/tyroocentral-platform/www/delivery/ckt.php?bannerid=17640394&affid=15927&subid1=&subid2=&subid3=&subid4=&subid5=&productid=ACCDCUC3STF7NPKD&optionalParams=&requestId=575fb5576b61c1465890135575fb5576b6cf&requestSource=PUBLIC&ad_group_id=-15927&gaid=d4f8030b-4a52-4261-9830-bd6c987cd261&IDFA=&dest=http://dl.flipkart.com/dl/philips-she-3590bk-98-headphone/p/itmdcudfaghsbmyj?pid=ACCDCUC3STF7NPKD&affid=svgmedia&affExtParam1=15927%3B%%trackerId%%%3B%%cookieId%%%3B%%subId1%%%3B%%subId2%%%3B%%subId3%%%3B%%subId4%%%3B%%subId5%%%3B&affExtParam2=%%cookieId%%'
		split 								= ckt.split('&dest=')
		cktInfo 							= split[0]# contains parameters of ckt
		cktInfo 							= urlparse.parse_qs(urlparse.urlparse(cktInfo).query,True)
		hardcodedParams['ExpectedDestURl'] 	= split[1]
		if 'subid1' in cktInfo:
			subID1 							= str(cktInfo['subid1']).strip('[]')
		if 'subid2' in cktInfo:
			subID2 							= str(cktInfo['subid2']).strip('[]')
		if 'subid3' in cktInfo:
			subID3 							= str(cktInfo['subid3']).strip('[]')
		if 'subid4' in cktInfo:
			subID4 							= str(cktInfo['subid4']).strip('[]')
		if 'subid5' in cktInfo:
			subID5 							= str(cktInfo['subid5']).strip('[]')
		if 'optionalParams' in cktInfo :
			optionalParams					= cktInfo['optionalParams']
		if 'IDFA' in cktInfo:
			IDFA 							= cktInfo['IDFA']
		if 'gaid' in cktInfo:
			gaid 							= cktInfo['gaid']
		hardcodedParams['ExpectedDestURl']	= hardcodedParams['ExpectedDestURl'].replace('%3B',';')
		edits 								= [("%%cookieId%%",str(hardcodedParams['cookies'])),("%%affiliateId%%",str(hardcodedParams['affiliateID'])),("%%bannerId%%",str(hardcodedParams['bannerID'])),("%%campaignId%%",str(hardcodedParams['campaignID'])),("%%poolId%%",str(hardcodedParams['zoneID'])),("%%subId1%%",str(subID1)),("%%subId2%%",str(subID2)),("%%subId3%%",str(subID3)),("%%subId4%%",str(subID4)),("%%subId5%%",str(subID5)),("%%optionalParam%%",str(optionalParams)),("%%trackerId%%",str(hardcodedParams['trackerID'])), ("%%gps_adid%%",str(gaid)), ("%%IDFA%%",str(IDFA)), ("%%clickId%%",str(click_ID))]
		for search, replace in edits:
	 		hardcodedParams['ExpectedDestURl']  	= hardcodedParams['ExpectedDestURl'].replace(search, replace)
	 		hardcodedParams['ExpectedDestURl'] 		= hardcodedParams['ExpectedDestURl'].replace("'","")


	# test to check url's are equal
	def test_matchURl(self):
		self.assertEqual(hardcodedParams['ExpectedDestURl'],hardcodedParams['actual_URL'],msg="url matching failed")

validCkt  		= ValidCkt()

if __name__ == '__main__':
    #unittest.main()
	valid   = ValidCkt()
	valid.getRedirectedData()
	unittest.main()
