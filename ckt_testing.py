#!/usr/bin/python
import urlparse
import MySQLdb
import urllib2
import cookielib
import time
import sys
import pymongo
import json
from pymongo import MongoClient
from pprint import pprint


# class NoRedirection is being used for interrupting redirection
class NoRedirection(urllib2.HTTPErrorProcessor):
    def http_response(self, request, response):
        return response

    https_response 		=		 http_response

#  this should be called when testing and this expects ckt as a argument by terminal for testing
def getCKT():
	subID1 = subID2 =subID3 = subID4 = subID5 = optionalParams = IDFA= gaid = bannerID =affiliateID=adgroupID=''
	accountid = zoneID = zoneDelievery = zoneStatus =msg = trackerID=discontinue_url=campaignID= cookie= click_ID =''
	ckt  				= sys.argv[1:]
	ckt 				= ''.join(ckt)
	reason_dict 		= {"0":"valid ckt","1":"user capping check failed","2":"affiliate details are empty","3":"campaign is expired","4":"targetting approval failed","5":"campaign approval failed","6":"campaign capping check is failed","7":"adwall is inactive","9":"banner id is int type or empty or bannerDetails are empty","10":"affiliate id is int type or empty"}
	#ckt 				= "http://localhost/tyroocentral-platform/www/delivery/ckt.php?bannerid=17640394&affid=15927&subid1=&subid2=&subid3=&subid4=&subid5=&productid=ACCDCUC3STF7NPKD&optionalParams=&requestId=575fb5576b61c1465890135575fb5576b6cf&requestSource=PUBLIC&ad_group_id=-15927&gaid=d4f8030b-4a52-4261-9830-bd6c987cd261&IDFA=&dest=http://dl.flipkart.com/dl/philips-she-3590bk-98-headphone/p/itmdcudfaghsbmyj?pid=ACCDCUC3STF7NPKD&affid=svgmedia&affExtParam1=15927%3B%%trackerId%%%3B%%cookieId%%%3B%%subId1%%%3B%%subId2%%%3B%%subId3%%%3B%%subId4%%%3B%%subId5%%%3B&affExtParam2=%%cookieId%%"
	par 				= urlparse.parse_qs(urlparse.urlparse(ckt).query)
	if 'bannerid' in par:
		bannerckt 		= par['bannerid']
		bannerID 		= ''.join(bannerckt)
	split 				= ckt.split('&dest=')
	cktInfo 			= split[0]# contains parameters of ckt
	cktInfo 			= urlparse.parse_qs(urlparse.urlparse(cktInfo).query,True)
	expectedDestURL 	= ''
	if "&dest" in ckt:
		expectedDestURL = split[1]# contains &dest part of ckt
	else :
		expectedDestURL = getDestURL(bannerID) # is destination url is not present ,gettingg it from ox_banners
	if 'affid'in cktInfo:
		affiliateID 	= cktInfo['affid']
		affiliateID  	= ''.join(affiliateID)
	if 'ad_group_id'in cktInfo:
		adgroupID 		= cktInfo['ad_group_id']
		adgroupID 		= ''.join(adgroupID)
	affiliateDetails 	= getAffiliateDetails(affiliateID,adgroupID)
	if affiliateDetails:
		accountid 		= affiliateDetails[0][1]
		zoneID 			= affiliateDetails[0][2]
		zoneDelievery 	= affiliateDetails[0][3]
		zoneStatus 		= affiliateDetails[0][4]
	else:
		affMsg 			= "affiliate details not found in db"
		msg 			=  msg+affMsg
	bannerDetails 		= getBannerDetails(bannerID)
	if bannerDetails:
		trackerID 		= bannerDetails[0][13]
		discontinue_url = bannerDetails[0][14]
		campaignID 		= bannerDetails[0][6]
	else :
		bannerMsg		= " ,banner details not found in db"
		msg 			= msg+bannerMsg
	actualData 			= getActualDestinationURL(ckt)
	if actualData:
		if 'cookie' in actualData:
			cookie 			= actualData['cookie']
			ut_cookie 		= actualData['ut_cookie']
		else:
			cookMsg 		= " ,cookies not found due to missing ckt parameters"
			msg 			= msg+cookMsg
		actDestinationUrl 	= actualData['url']
	if 'subid1' in cktInfo:
		subID1 			= str(cktInfo['subid1']).strip('[]')
	if 'subid2' in cktInfo:
		subID2 			= str(cktInfo['subid2']).strip('[]')
	if 'subid3' in cktInfo:
		subID3 			= str(cktInfo['subid3']).strip('[]')
	if 'subid4' in cktInfo:
		subID4 			= str(cktInfo['subid4']).strip('[]')
	if 'subid5' in cktInfo:
		subID5 			= str(cktInfo['subid5']).strip('[]')
	if 'optionalParams' in cktInfo :
		optionalParams	= cktInfo['optionalParams']
	if 'IDFA' in cktInfo:
		IDFA 			= cktInfo['IDFA']
	if 'gaid' in cktInfo:
		gaid 			= cktInfo['gaid']
	clickReason 		= getClickID(affiliateID,bannerID)
	if clickReason:
		click_ID 			= clickReason['click_ID']
		reason_Redirection 	= clickReason['reason_redirection']
	else :
		clickMsg            = " ,clickID details not found in mongo"
		msg 				= msg+clickMsg
	expectedDestURL	    = expectedDestURL.replace('%3B',';')
	edits 				= [("%%cookieId%%",cookie),("%%affiliateId%%",str(affiliateID)),("%%bannerId%%",str(bannerID)),("%%campaignId%%",str(campaignID)),("%%poolId%%",str(zoneID)),("%%subId1%%",str(subID1)),("%%subId2%%",str(subID2)),("%%subId3%%",str(subID3)),("%%subId4%%",str(subID4)),("%%subId5%%",str(subID5)),("%%optionalParam%%",str(optionalParams)),("%%trackerId%%",str(trackerID)), ("%%gps_adid%%",str(gaid)), ("%%IDFA%%",str(IDFA)), ("%%clickId%%",str(click_ID))]
	for search, replace in edits:
	 	expectedDestURL  	= expectedDestURL.replace(search, replace)
	 	expectedDestURL 	= expectedDestURL.replace("'","")
	if actDestinationUrl 	== expectedDestURL:
		validResult      	= getDocumentfromMongo(ut_cookie,reason_dict)
		pprint(validResult)
		msg = "ckt is valid and logged successfully with all attributes given above"
		print msg
	else :
		if actDestinationUrl 	!= expectedDestURL:
			actFaultPageUrlwithParams 	= actDestinationUrl.split("&")[0]
			actFaultPageUrlNoParams 	= actDestinationUrl.split("?")[0]
			invalidResult 				= getDocumentfromMongo(ut_cookie,reason_dict)
			reason 						= invalidResult['reason_for_redirection']
			reason 						= str(reason)
			#pprint (invalidResult)
			if (reason == '9')or(reason =='2')or(reason=='10'):
				globalUrl 				= getGlobalUrl()
				globalUrlWithParam 		= globalUrl.replace('%%reasonForRedirection%%',reason)
				globalUrlNoParam 		= globalUrl.split('?')[0]
				if (actFaultPageUrlwithParams == globalUrlWithParam) or (actFaultPageUrlNoParams==globalUrlNoParam):
					print " \nfault page url matched with global url"
					print '\n'+'reason:'+reason+' '+reason_dict[reason]
				else :
					print " fault page url Mismatched with global url"
					print '\n'+'reason:'+reason+' '+reason_dict[reason]
			else:
				if (reason == '1') or (reason=='3')or (reason=='4') or (reason=='5') or (reason=='6')or (reason=='7'):
					geoUrl 					= getGeoUrl(campaignID,reason,affiliateID)
					geoUrlWithparam 		= geoUrl.replace('%%reasonForRedirection%%',reason)
					geoUrlNoparam 			= geoUrl.split('?')[0]
					if (actFaultPageUrlwithParams == geoUrlWithparam) or (actFaultPageUrlNoParams == geoUrlNoparam):
						print "\n geo URL is matched with fault page url"
						print '\n'+'reason:'+reason+' '+reason_dict[reason]
					else :
						discontinue_url 			= discontinue_url.split('&')[0]
						discontinue_urlWithParam 	= discontinue_url.replace('%%reasonForRedirection%%',reason)
						discontinue_urlNoParam 		= discontinue_url.split('?')[0]
						if (actFaultPageUrlwithParams  == discontinue_urlWithParam) or (actFaultPageUrlNoParams == discontinue_urlNoParam):
							print " \ndiscontinue url matched with fault page url"
							print '\n'+'reason:'+reason+' '+reason_dict[reason]
						else :
							globalUrl 				= getGlobalUrl()
							globalUrlWithParam 		= globalUrl.replace('%%reasonForRedirection%%',reason)
							globalUrlNoParam 		= globalUrl.split('?')[0]
							if (actFaultPageUrlwithParams == globalUrlWithParam) or (actFaultPageUrlNoParams==globalUrlNoParam):
								print "\n fault page url matched with global url"
								print '\n'+'reason:'+reason+' '+reason_dict[reason]
							else:
								print "\n fault page url Mismatched with any kind of url url"
								print '\n'+'reason:'+reason+' '+reason_dict[reason]

		print msg

# method to get geoUrl in terms of redirection fault page
def getGeoUrl(campaignID,reason,affiliateID):
	collection 			= 'ox_raw_clicks_new'
	condition 			= {"campaign_id":str(campaignID),"affiliate_id":str(affiliateID),"reason_for_redirection":str(reason)}
	result 				= getMongoConnection(collection,condition)
	for r in result:
		countryCode     = r['country']
		countryCode 	= str(countryCode)
	query 				= 'SELECT url FROM ox_georedirection WHERE campaignid = "%s" AND country = "%s"' %(campaignID,countryCode)
	response 			= []
	response 			= ''.join(response[0])
	response 			= response.split('&')[0]
	return query

# methos to get globab url
def getGlobalUrl():
	query 				= 'SELECT url FROM ox_globalredirection'
	response 			= []
	response 			=  getMysqlResult(query)
	response 			= ''.join(response[0])
	response 			= response.split('&')[0]
	return response

# method for retrieving document for given ut_cookie and comparing all parameters for valid url
def getDocumentfromMongo(ut_cookie,reason_dict):
	collection  		= 'ox_raw_clicks_new'
	condition 			=  {"userTrackingCookie":str(ut_cookie)}
	result 				= getMongoConnection(collection,condition)
	for r in result:
		reason 			= r['reason_for_redirection']
		reason 			= str(reason)
		if reason in reason_dict:
			r[reason] = reason_dict[reason]
		newResult = r	
	return newResult

# method for getting clickID logged into table ox_raw_clicks_new
def getClickID(affiliateID,bannerID):
	collection			= 'ox_raw_clicks_new'
	condition 			= {"ad_id":str(bannerID),"affiliate_id": affiliateID}
	result 				= getMongoConnection(collection,condition)
	response 			= {}
	for document in result:
		if document:
			click_ID  			= document['click_id']
	 		reason_redirection 	= document['reason_for_redirection']
	 		countryCode 		= document['country']
	 		response 			= {"click_ID" : click_ID,"reason_redirection":reason_redirection,"countryCode":countryCode}
	return response  
	

# method to get actual destination url after hitting ckt
def getActualDestinationURL(ckt):
	cookies 			= {}
	result 				= {}
	cj 					= cookielib.CookieJar()
	opener 				= urllib2.build_opener(NoRedirection, urllib2.HTTPCookieProcessor(cj))
	response 			= opener.open(ckt)
	for cookie in cj:
   		cookies[cookie.name] 	= cookie.value
   	if cookies:
   			oaid_cookie 		= cookies['OAID']
			ut_cookie 			= cookies['__tyroo_ut']
			actCookie 			= oaid_cookie+'_UT_'+ut_cookie
			result['cookie'] 	= actCookie
			result['ut_cookie']	= ut_cookie
	if response.code 	== 302:
		redirection_target 	= response.headers['Location']
		result['url'] 		= redirection_target
	time.sleep(1)
	return result


# gets banner details by given bannerID
def getBannerDetails(bannerID):
	query = """SELECT b.bannerid, b.url_mobile, b.url, b.compiledlimitation, b.acl_plugins,c.enable_targeting, c.campaignid, 
				c.visibility, c.deeplink_query_string,c.status, c.deeplink_condition, c.cookietime, c.clientid, 
				t.trackerid, c.discontinue_url, c.expire_time,c.views as total_impressions, c.clicks as total_clicks,c.conversions as total_conversions,c.daily_impressions, c.daily_clicks, c.daily_conversions, 
				c.monthly_impressions, c.monthly_clicks, c.monthly_conversions FROM ox_banners as b 
				INNER JOIN ox_campaigns as c  ON (b.campaignid = c.campaignid)INNER JOIN  ox_campaigns_trackers as t ON (t.campaignid = c.campaignid)
				WHERE b.bannerid = "%s";""" % (bannerID)
	bannerDetail  		= getMysqlResult(query)# get result from mysql 's openx db'
	bannerdet 			= list(bannerDetail)
	return bannerdet

# gets affiliate details
def getAffiliateDetails(affiliateID,adgroupID):
	query 				= """SELECT a.affiliateid, a.account_id as accountid, z.zoneid as zone_id, z.delivery, z.status FROM ox_affiliates as a RIGHT OUTER JOIN ox_zones as z ON (a.affiliateid = z.affiliateid) WHERE (a.affiliateid = "%s" AND z.delivery = '-1') OR (z.zoneid = "%s");""" % (affiliateID,adgroupID)
	affiliateDetail 	= getMysqlResult(query)
	affDetails 			= list(affiliateDetail)
	return affDetails
	
# gets expected destination url if not given in ckt
def getDestURL(bannerID):
	query 				= 'select url from ox_banners where bannerid ="%s";'% bannerID
	dest  				= getMysqlResult(query)
	return dest

# method connects with mysql instance
def getMysqlResult(query):
	#dbSql=MySQLdb.connect(host='internal-galera-db-592150868.ap-southeast-1.elb.amazonaws.com',user='code_user',passwd='n3wrl4l43l1c004777cm4ty',db='openx')
	dbSql 				= MySQLdb.connect(host='localhost',user='root',passwd='tyroo123',db='openx')
	cursor 				= dbSql.cursor()
	cursor.execute(query)
	dest 				= cursor.fetchall()
	return dest

# making conncetion from mongoDB instance
def getMongoConnection(collection,condition):
	client 				= MongoClient()
	db 					= client['openx']
	collection 			= db[collection]
	response 			= collection.find(condition).sort("_id",pymongo.DESCENDING).limit(1)# getting latest document
	return response
	
	

if __name__ == "__main__":
	noRe = NoRedirection()
	getCKT()

