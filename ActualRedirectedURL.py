import urllib2
import cookielib
# class NoRedirection(urllib2.HTTPErrorProcessor):
#     def http_response(self, request, response):
#         return response

#     https_response 		= 	http_response
def getActualUrl(noRe):
	cookies 			= {}
	result 			   	= {}
	cj 				   	= cookielib.CookieJar()
	result['coolieObj'] = cj
	opener 			   	= urllib2.build_opener(noRe, urllib2.HTTPCookieProcessor(cj))
	ckt 			   	= "http://localhost/tyroocentral-platform/www/delivery/ckt.php?bannerid=17640394&affid=15927&subid1=&subid2=&subid3=&subid4=&subid5=&productid=ACCDCUC3STF7NPKD&optionalParams=&requestId=575fb5576b61c1465890135575fb5576b6cf&requestSource=PUBLIC&ad_group_id=-15927&gaid=d4f8030b-4a52-4261-9830-bd6c987cd261&IDFA=&dest=http://dl.flipkart.com/dl/philips-she-3590bk-98-headphone/p/itmdcudfaghsbmyj?pid=ACCDCUC3STF7NPKD&affid=svgmedia&affExtParam1=15927%3B%%trackerId%%%3B%%cookieId%%%3B%%subId1%%%3B%%subId2%%%3B%%subId3%%%3B%%subId4%%%3B%%subId5%%%3B&affExtParam2=%%cookieId%%"
	response 		   	= opener.open(ckt)
	for cookie in cj:
   		cookies[cookie.name] 	= cookie.value
   	if cookies:
   			oaid_cookie 		= cookies['OAID']
			ut_cookie 			= cookies['__tyroo_ut']
			actCookie 			= oaid_cookie+'_UT_'+ut_cookie
			result['cookie'] 	= actCookie
			result['ut_cookie']	= ut_cookie
	if response.code   == 302:
		redirection_target 	= response.headers['Location']
		result["targetURL"]	= redirection_target 
	return result